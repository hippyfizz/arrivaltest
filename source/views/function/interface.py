from database import Interface
from views.mixins import MixinListView, MixinDetailView
from views.router import routes


@routes.view('/api/v1/interface/')
class InterfaceListView(MixinListView):
    class Meta:
        model = Interface
        fields = ['id',
                  'name',
                  {
                      'functions': ['id',
                                    'name']
                  },
                  'created_at',
                  'deleted_at']


@routes.view('/api/v1/interface/{id}/')
class InterfaceDetailView(MixinDetailView):
    class Meta:
        model = Interface
        fields = ['id', 'name']
