from database import Component
from views.mixins import MixinListView, MixinDetailView
from views.router import routes


@routes.view('/api/v1/component/')
class ComponentListView(MixinListView):
    class Meta:
        model = Component
        fields = ['id',
                  'name',
                  {
                      'location': ['id',
                                   'name']
                  }]

    async def get(self, **kwargs):
        return await super().get(**kwargs)


@routes.view('/api/v1/component/{id}/')
class ComponentDetailView(MixinDetailView):
    class Meta:
        model = Component
        fields = ['id', 'name']
