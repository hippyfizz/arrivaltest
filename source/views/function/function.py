from database import Function
from views.mixins import MixinDetailView, MixinListView
from views.router import routes


@routes.view('/api/v1/function/')
class FunctionListView(MixinListView):
    class Meta:
        model = Function
        fields = ['id',
                  'name',
                  {
                      'component': ['id',
                                    'name']
                  },
                  {
                      'interfaces': ['id',
                                     'name']
                  }]


@routes.view('/api/v1/function/{id}/')
class FunctionDetailView(MixinDetailView):
    class Meta:
        model = Function
        fields = ['id', 'name']
