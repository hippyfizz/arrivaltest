from database import Location
from views.mixins import MixinDetailView, MixinListView
from views.router import routes


@routes.view('/api/v1/location/')
class LocationListView(MixinListView):
    class Meta:
        model = Location
        fields = ['id',
                  'name']


@routes.view('/api/v1/location/{id}/')
class LocationDetailView(MixinDetailView):
    class Meta:
        model = Location
        fields = ['id',
                  'name']
