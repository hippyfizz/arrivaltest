from .component import ComponentListView, ComponentDetailView
from .function import FunctionListView, FunctionDetailView
from .interface import InterfaceListView, InterfaceDetailView
from .location import LocationListView, LocationDetailView

