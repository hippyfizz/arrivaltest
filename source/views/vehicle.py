from aiohttp import web

from views.router import routes


@routes.view('/vehicle')
class VehicleView(web.View):
    async def get(self):
        return web.Response(text="ping")

    async def post(self):
        return web.Response(text="pong")