from database.vehicle import Vehicle
from views import FeatureListView, LocationListView
from views.mixins import MixinListView, MixinDetailView
from views.router import routes


@routes.view('/api/v1/vehicle/')
class LocationListView(MixinListView):
    class Meta:
        model = Vehicle
        fields = ['id',
                  'name']


@routes.view('/api/v1/vehicle/{id}/')
class LocationDetailView(MixinDetailView):
    class Meta:
        model = Vehicle
        fields = ['id',
                  'name',
                  {
                      'locations': LocationListView.Meta.fields
                  },
                  {
                      'features': FeatureListView.Meta.fields
                  }]
