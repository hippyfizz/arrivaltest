from abc import ABCMeta
from typing import List, Union, Dict

from aiohttp import web
from sqlalchemy import select


class MixinMeta(ABCMeta):
    _model = None
    _fields: Union[List[str], Dict[str, List[str]]] = None

    def __new__(cls, name, bases, namespace, **kwargs):
        meta = namespace.get('Meta')
        if meta:
            namespace['_model'] = meta.model
            namespace['_fields'] = meta.fields
        return super().__new__(cls, name, bases, namespace, **kwargs)


class MixinListView(web.View, metaclass=MixinMeta):
    _model = None
    _fields: Union[List[str], Dict[str, List[str]]] = None

    async def get(self, **kwargs):
        async with self.request.app['sessionmaker']() as session:
            if 'query' in kwargs:
                query = kwargs['query']
            else:
                query = select(self._model)
            results = await session.execute(query)
            response = list(map(lambda result: result.serialize(self._fields), results.unique().scalars()))
            await session.commit()
        return web.json_response(response)


class MixinDetailView(web.View, metaclass=MixinMeta):
    _model = None
    _fields: Union[List[str], Dict[str, List[str]]] = None

    async def get(self, **kwargs):
        async with self.request.app['sessionmaker']() as session:
            try:
                object_id = int(self.request.match_info['id'])
            except ValueError:
                raise web.HTTPNotFound()
            if 'query' in kwargs:
                query = kwargs['query']
            else:
                query = select(self._model)
            query = query.filter(self._model.id == object_id)
            result = await session.execute(query)
            data = result.unique().first()
            await session.commit()
        if data:
            return web.json_response(data.serialize(self._fields))
        raise web.HTTPNotFound()
