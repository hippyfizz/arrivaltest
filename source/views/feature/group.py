from database import FeatureGroup
from views.mixins import MixinListView, MixinDetailView
from views.router import routes


@routes.view('/api/v1/feature_group/')
class FeatureGroupListView(MixinListView):
    class Meta:
        model = FeatureGroup
        fields = ['id',
                  'name',
                  {
                      'subgroups': ['id',
                                    'name',
                                    'is_set']
                  },
                  {
                      'features': ['id',
                                   'name',
                                   'group',
                                   'subgroup',
                                   {
                                       'functions': ['id',
                                                     'name',
                                                     {
                                                         'component': ['id',
                                                                       'name']
                                                     },
                                                     {
                                                         'interfaces': ['id',
                                                                        'name',
                                                                        'created_at',
                                                                        'deleted_at']
                                                     }]
                                   }]
                  }]


@routes.view('/api/v1/feature_group/{id}/')
class FeatureGroupDetailView(MixinDetailView):
    class Meta:
        model = FeatureGroup
        fields = ['id',
                  'name',
                  'subgroups']
