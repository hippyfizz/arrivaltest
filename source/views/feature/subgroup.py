from database import FeatureSubgroup
from views.mixins import MixinListView, MixinDetailView
from views.router import routes


@routes.view('/api/v1/feature_subgroup/')
class FeatureGroupListView(MixinListView):
    class Meta:
        model = FeatureSubgroup
        fields = ['id',
                  'name',
                  'is_set',
                  'group',
                  'features']


@routes.view('/api/v1/feature_subgroup/{id}/')
class FeatureGroupDetailView(MixinDetailView):
    class Meta:
        model = FeatureSubgroup
        fields = ['id',
                  'name',
                  'is_set',
                  'group']
