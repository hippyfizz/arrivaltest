from database import Feature
from views.mixins import MixinListView, MixinDetailView
from views.router import routes


@routes.view('/api/v1/feature/')
class FeatureListView(MixinListView):
    class Meta:
        model = Feature
        fields = ['id',
                  'name',
                  'group',
                  'subgroup',
                  {
                      'functions': ['id',
                                    'name',
                                    {
                                        'component': ['id',
                                                      'name']
                                    },
                                    {
                                        'interfaces': ['id',
                                                       'name']
                                    }]
                  }]


@routes.view('/api/v1/feature/{id}/')
class FeatureDetailView(MixinDetailView):
    class Meta:
        model = Feature
        fields = ['id',
                  'name',
                  'group',
                  'subgroup']
