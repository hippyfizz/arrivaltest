from sqlalchemy import Column, String, Table, ForeignKey, Integer
from sqlalchemy.orm import relationship

from database.base import Base

vehicle_location_association_table = Table(
    'vehicle_location_association',
    Base.metadata,
    Column('vehicles_id', Integer, ForeignKey('vehicles.id')),
    Column('location_id', Integer, ForeignKey('locations.id'))
)

vehicle_feature_association_table = Table(
    'vehicle_feature_association',
    Base.metadata,
    Column('vehicles_id', Integer, ForeignKey('vehicles.id')),
    Column('feature_id', Integer, ForeignKey('features.id'))
)


class Vehicle(Base):
    __tablename__ = "vehicles"

    name = Column(String)
    locations = relationship(
        "Location",
        secondary=vehicle_location_association_table,
        lazy='joined'
    )
    features = relationship(
        "Feature",
        secondary=vehicle_feature_association_table,
        lazy='joined'
    )
