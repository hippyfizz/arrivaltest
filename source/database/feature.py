from sqlalchemy import Column, String, ForeignKey, Table, Integer, Boolean
from sqlalchemy.orm import relationship, validates

from database import Base


class FeatureGroup(Base):
    __tablename__ = 'feature_groups'

    name = Column(String, unique=True, nullable=False)
    subgroups = relationship("FeatureSubgroup",  back_populates='group', lazy='joined')
    features = relationship("Feature", back_populates='group', lazy='joined')


class FeatureSubgroup(Base):
    __tablename__ = 'feature_subgroups'

    name = Column(String, unique=True, nullable=False)
    is_set = Column(Boolean)
    group_id = Column(ForeignKey('feature_groups.id'), nullable=False)
    group = relationship("FeatureGroup", back_populates='subgroups', lazy='joined')
    features = relationship("Feature", back_populates='subgroup', lazy='joined')


feature_function_association_table = Table(
    'feature_function_association_table',
    Base.metadata,
    Column('features_id', Integer, ForeignKey('features.id')),
    Column('function_id', Integer, ForeignKey('functions.id'))
)


class Feature(Base):
    __tablename__ = 'features'
    name = Column(String, unique=True, nullable=False)
    group_id = Column(ForeignKey('feature_groups.id'), nullable=True)
    group = relationship("FeatureGroup", back_populates='features', lazy='joined')
    subgroup_id = Column(ForeignKey('feature_subgroups.id'), nullable=True)
    subgroup = relationship("FeatureSubgroup", back_populates='features', lazy='joined')
    functions = relationship(
        "Function",
        secondary=feature_function_association_table,
        lazy='joined'
    )

    @validates('group', 'subgroup')
    def validate_groups(self, key, value):
        """
        :param string key: значение поля для валилации может быть group или subgroup
        :param datetime value: может принимать значения FeatureGroup, FeatureSubgroup или NoneType
        :return: возвращает полученный value в случае успеха
        :rtype: Union[FeatureGroup, FeatureSubgroup, None]
        :raises AssertionError: в случае нарушения бизнес логики приложения
        """
        if key == 'group':
            if value is None:
                if self.subgroup is None:
                    raise AssertionError('You must set group or subgroup.')
            else:
                if self.subgroup is not None:
                    raise AssertionError('You cannot set group and subgroup together.')
        elif key == 'subgroup':
            if value is None:
                if self.group is None:
                    raise AssertionError('You must set group or subgroup.')
            else:
                if self.group is not None:
                    raise AssertionError('You cannot set group and subgroup together.')
        return value


