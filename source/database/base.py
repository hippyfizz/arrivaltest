import datetime
import json
from typing import List, Optional, Dict, Any, Union

from sqlalchemy import Column, Integer, DateTime, func
from sqlalchemy.orm import declarative_base, declared_attr


def _is_jsonable(x):
    try:
        json.dumps(x)
        return True
    except (TypeError, OverflowError):
        return False


class BaseModel(object):
    _fields: Union[List[str], Dict[str, List[str]]] = ['id', 'created_at', 'deleted_at']

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, server_default=func.now())
    deleted_at = Column(DateTime, nullable=True, default=None)

    def serialize(self, fields: Optional[Union[Dict[str, List[str]], List[str]]] = None):
        """
        по хорошему нужно создать отдельный класс для сериализации и десериализации объектов,
        но в рамках тестового задания такой простой вариант считаю достаточным
        :param list fields: список полей, которые должны участвовать в сереализации
        :return dict: валидные поля для отдачи в JSON формате
        """
        data: Dict[str, Any] = {}
        if not fields:
            fields = self._fields
        for field in fields:
            if type(field) is dict:
                key = list(field.keys()).pop()
            else:
                key = field
            value = getattr(self, key)
            if type(value) is datetime.datetime:
                value = str(value)
            elif issubclass(type(value), BaseModel) or (hasattr(value, '__iter__') and all(issubclass(type(item), BaseModel) for item in value)):
                related_fields = None
                if type(field) is dict:
                    related_fields = field.get(key, None)
                if hasattr(value, '__iter__'):
                    value = list(map(lambda related_obj: related_obj.serialize(related_fields), value))
                else:
                    value = value.serialize(related_fields)
            else:
                if not _is_jsonable(value):
                    continue
            data[key] = value
        return data


Base = declarative_base(cls=BaseModel)
