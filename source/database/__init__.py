import os

from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker

from database.base import Base
from .feature import FeatureGroup, FeatureSubgroup, Feature
from .function import Function, Location, Component, Interface
from .vehicle import Vehicle


async def get_async_session(expire_on_commit=False):
    engine = create_async_engine(
        f'postgresql+asyncpg://{os.environ.get("DB_USERNAME")}:{os.environ.get("DB_PASSWORD")}'
        f'@{os.environ.get("DB_HOST")}/{os.environ.get("DB_NAME")}',
        echo=True,
    )

    async_session = sessionmaker(
        engine,
        expire_on_commit=expire_on_commit,
        class_=AsyncSession
    )
    return async_session
