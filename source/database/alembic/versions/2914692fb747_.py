"""empty message

Revision ID: 2914692fb747
Revises: 5b00bda4cb9f
Create Date: 2021-05-18 10:51:00.363209

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2914692fb747'
down_revision = '5b00bda4cb9f'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('features', sa.Column('group_id', sa.Integer(), nullable=True))
    op.add_column('features', sa.Column('subgroup_id', sa.Integer(), nullable=True))
    op.drop_constraint('features_group_fkey', 'features', type_='foreignkey')
    op.drop_constraint('features_subgroup_fkey', 'features', type_='foreignkey')
    op.create_foreign_key(None, 'features', 'feature_subgroups', ['subgroup_id'], ['id'])
    op.create_foreign_key(None, 'features', 'feature_groups', ['group_id'], ['id'])
    op.drop_column('features', 'subgroup')
    op.drop_column('features', 'group')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('features', sa.Column('group', sa.INTEGER(), autoincrement=False, nullable=True))
    op.add_column('features', sa.Column('subgroup', sa.INTEGER(), autoincrement=False, nullable=True))
    op.drop_constraint(None, 'features', type_='foreignkey')
    op.drop_constraint(None, 'features', type_='foreignkey')
    op.create_foreign_key('features_subgroup_fkey', 'features', 'feature_subgroups', ['subgroup'], ['id'])
    op.create_foreign_key('features_group_fkey', 'features', 'feature_groups', ['group'], ['id'])
    op.drop_column('features', 'subgroup_id')
    op.drop_column('features', 'group_id')
    # ### end Alembic commands ###
