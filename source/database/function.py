from sqlalchemy import String, Column, ForeignKey, Table, Integer
from sqlalchemy.orm import relationship

from database.base import Base


function_interface_association_table = Table(
    'function_interface_association',
    Base.metadata,
    Column('function_id', Integer, ForeignKey('functions.id')),
    Column('interface_id', Integer, ForeignKey('interfaces.id'))
)


class Function(Base):
    __tablename__ = 'functions'
    name = Column(String, unique=True, nullable=False)
    component_id = Column(ForeignKey('components.id'))
    component = relationship("Component", lazy='joined')
    interfaces = relationship(
        "Interface",
        secondary=function_interface_association_table,
        back_populates='functions',
        lazy='joined'
    )


class Location(Base):
    __tablename__ = 'locations'
    name = Column(String, unique=True, nullable=False)
    components = relationship("Component", back_populates="location", lazy='joined')


class Component(Base):
    __tablename__ = 'components'
    name = Column(String, unique=True, nullable=False)
    location_id = Column(ForeignKey("locations.id"))
    location = relationship("Location", back_populates="components", lazy='joined')


class Interface(Base):
    __tablename__ = 'interfaces'
    name = Column(String, unique=True, nullable=False)
    functions = relationship(
        'Function',
        secondary=function_interface_association_table,
        back_populates='interfaces',
        lazy='joined'
    )

