from aiohttp import web

from database import get_async_session
from views.router import routes


async def init_app():
    app = web.Application()
    app['sessionmaker'] = await get_async_session()
    app.add_routes(routes)
    return app

web.run_app(init_app())
