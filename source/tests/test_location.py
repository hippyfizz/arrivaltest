from aiohttp.test_utils import unittest_run_loop

from tests import ArrivalAppTestCase
from tests.factories.factories import LocationFactory


class LocationApiTestCase(ArrivalAppTestCase):
    def get_mocked_session(self):
        self.return_values = LocationFactory.build_batch(4)
        return super().get_mocked_session()

    @unittest_run_loop
    async def test_list_locations(self):
        resp = await self.client.request("GET", "/api/v1/location/")
        assert resp.status == 200
        data = await resp.json()
        assert len(self.return_values) == len(data)
