from typing import List, Any
from unittest.mock import AsyncMock, MagicMock, Mock

import pytest
from aiohttp import web
from aiohttp.test_utils import AioHTTPTestCase

from database import get_async_session
from tests.factories import Session, ModelFactory
from tests.factories.factories import InterfaceFactory
from views.router import routes


class ArrivalAppTestCase(AioHTTPTestCase):
    return_values: List[Any] = None

    def get_mocked_session(self):
        """
        мок для сессии базы данных, через значение return_values можно подсовывать
        замоканные записи в БД для отдачи в API
        :return MagicMock:
        """
        result_mock = Mock()
        result_mock.unique.return_value = result_mock
        result_mock.scalars.return_value = self.return_values

        session_mock = AsyncMock()
        session_mock.return_value = session_mock
        session_mock.execute.return_value = result_mock

        context_manager_mock = MagicMock()
        context_manager_mock.return_value = context_manager_mock
        context_manager_mock.__aenter__.return_value = session_mock
        return context_manager_mock

    async def get_application(self):
        app = web.Application()
        app['sessionmaker'] = self.get_mocked_session()
        app.add_routes(routes)
        return app
