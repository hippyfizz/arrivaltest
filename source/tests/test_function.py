from aiohttp.test_utils import unittest_run_loop

from tests import ArrivalAppTestCase
from tests.factories.factories import FunctionFactory


class FunctionApiTestCase(ArrivalAppTestCase):
    def get_mocked_session(self):
        self.return_values = FunctionFactory.build_batch(4)
        return super().get_mocked_session()

    @unittest_run_loop
    async def test_list_functions(self):
        resp = await self.client.request("GET", "/api/v1/function/")
        assert resp.status == 200
        data = await resp.json()
        assert len(self.return_values) == len(data)
