from aiohttp.test_utils import unittest_run_loop

from tests import ArrivalAppTestCase
from tests.factories.factories import NonSetSubgroupFactory, SetSubgroupFactory


class NonSetSubgroupApiTestCase(ArrivalAppTestCase):
    def get_mocked_session(self):
        self.return_values = NonSetSubgroupFactory.build_batch(4)
        return super().get_mocked_session()

    @unittest_run_loop
    async def test_list_nonset_feature_subgroups(self):
        resp = await self.client.request("GET", "api/v1/feature_subgroup/")
        assert resp.status == 200
        data = await resp.json()
        assert len(self.return_values) == len(data)


class SetSubgroupApiTestCase(ArrivalAppTestCase):
    def get_mocked_session(self):
        self.return_values = SetSubgroupFactory.build_batch(4)
        return super().get_mocked_session()

    @unittest_run_loop
    async def test_list_set_feature_subgroups(self):
        resp = await self.client.request("GET", "api/v1/feature_subgroup/")
        assert resp.status == 200
        data = await resp.json()
        assert len(self.return_values) == len(data)
