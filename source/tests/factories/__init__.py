from factory.alchemy import SQLAlchemyModelFactory
from sqlalchemy import orm

Session = orm.scoped_session(orm.sessionmaker())


class ModelFactory(SQLAlchemyModelFactory):
    class Meta:
        abstract = True
        sqlalchemy_session = Session
