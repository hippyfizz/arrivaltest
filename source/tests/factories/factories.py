import factory

from database import Interface, Location, Function, Component, FeatureGroup, FeatureSubgroup, Feature, Vehicle
from tests.factories import ModelFactory


class FeatureFactory(ModelFactory):
    class Meta:
        model = Feature

    id = factory.Sequence(lambda n: n + 1)
    name = factory.Faker('name')
    group = factory.RelatedFactory('tests.factories.factories.FeatureGroupFactory')
    subgroup = factory.RelatedFactory('tests.factories.factories.NonSetSubgroupFactory')
    functions = factory.RelatedFactoryList('tests.factories.factories.FunctionFactory')


class FunctionFactory(ModelFactory):
    class Meta:
        model = Function

    id = factory.Sequence(lambda n: n + 1)
    name = factory.Faker('name')


class FeatureGroupFactory(ModelFactory):
    class Meta:
        model = FeatureGroup

    name = factory.Faker('name')
    # subgroups = factory.RelatedFactoryList('tests.factories.factories.NonSetSubgroupFactory')
    # features = factory.RelatedFactoryList('tests.factories.factories.FeatureFactory')


class NonSetSubgroupFactory(ModelFactory):
    class Meta:
        model = FeatureSubgroup

    id = factory.Sequence(lambda n: n + 1)
    name = factory.Faker('name')
    is_set = False
    # group = factory.RelatedFactory('tests.factories.factories.FeatureGroupFactory')
    # features = factory.RelatedFactoryList('tests.factories.factories.FeatureFactory')


class SetSubgroupFactory(ModelFactory):
    class Meta:
        model = FeatureSubgroup

    id = factory.Sequence(lambda n: n + 1)
    name = factory.Faker('name')
    is_set = True
    group = factory.RelatedFactory('tests.factories.factories.FeatureGroupFactory')
    features = factory.RelatedFactoryList('tests.factories.factories.FeatureFactory')


class LocationFactory(ModelFactory):
    class Meta:
        model = Location

    id = factory.Sequence(lambda n: n + 1)
    name = factory.Faker('name')
    components = factory.RelatedFactoryList('tests.factories.factories.FunctionFactory')


class ComponentFactory(ModelFactory):
    class Meta:
        model = Component

    id = factory.Sequence(lambda n: n + 1)
    name = factory.Faker('name')
    location = factory.RelatedFactory(LocationFactory)
    # factory.RelatedFactoryList


class InterfaceFactory(ModelFactory):
    class Meta:
        model = Interface

    id = factory.Sequence(lambda n: n + 1)
    name = factory.Faker('name')
    functions = factory.RelatedFactoryList('tests.factories.factories.FunctionFactory')


class VehicleFactory(ModelFactory):
    class Meta:
        model = Vehicle

    id = factory.Sequence(lambda n: n + 1)
    name = factory.Faker('name')
    # features = factory.RelatedFactoryList('tests.factories.factories.FeatureFactory')
