FROM python:3.9.5-slim

RUN apt update && apt install -y python3-dev gcc g++ libcurl4-openssl-dev libssl-dev libpq-dev musl-dev
RUN mkdir -p /app
WORKDIR /app
ENV PYTHONPATH=/app:$PYTHONPATH

RUN pip install alembic psycopg2-binary poetry
COPY pyproject.toml .
COPY poetry.lock .

RUN poetry install

COPY source/ .